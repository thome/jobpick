#!/bin/bash

set -e
# set -x

job_queue_path=$HOME/jobpick
mode="job"
scheduler="OAR"
cpumapper="hwloc"
me=$(realpath $0)
repeat=1
afterjob_delay=5
subdivide_tasks=1
job_weight=1
: ${cwd="$PWD"}
keep_args=()

while [ $# -gt 0 ] ; do
    if [ "$1" = "--job-queue-path" ] ; then
        job_queue_path="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--scheduler" ] ; then
        scheduler="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--repeat" ] ; then
        repeat="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--afterjob-delay" ] ; then
        afterjob_delay="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--cpumapper" ] ; then
        cpumapper="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--cwd" ] ; then
        cwd="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--job-weight" ] ; then
        job_weight="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--subdivide-tasks" ] ; then
        subdivide_tasks="$2"
        keep_args=("${keep_args[@]}" "$1" "$2")
        shift; shift
    elif [ "$1" = "--mode" ] ; then
        mode="$2"
        # do not keep this.
        shift ; shift
    # maybe add binding info
    # maybe add provision for adaptive jobs that react on the platform
    # specifics
    elif [[ "$1" =~ ^- ]] ; then
        echo "Unrecognized option $*" >&2
        exit 1
    else
        script_args=("$@")
        break
    fi
done

TODO_DIR="$job_queue_path/todo"
DOING_DIR="$job_queue_path/doing"
DONE_DIR="$job_queue_path/done"
FAILED_DIR="$job_queue_path/failed"


do_setup() {
    mkdir "$TODO_DIR" || :
    mkdir "$DOING_DIR" || :
    mkdir "$DONE_DIR" || :
    mkdir "$FAILED_DIR" || :
}

do_job() {
    if [ "$scheduler" = "OAR" ] ; then
        if ! [ "$OAR_NODEFILE" ] ; then
            echo "No \$OAR_NODEFILE variable. Not within a job ?" >&2
            exit 1
        fi
        for node in $(uniq $OAR_NODEFILE) ; do
            echo "Spawning on $node"
            escaped_args=()
            for x in "${script_args[@]}" ; do
                escaped_args=("${escaped_args[@]}" `printf '%q' "$x"`)
            done
            oarsh -qx $node "$me ${keep_args[@]} --mode node --cwd ${cwd} ${escaped_args[@]}" &
        done
        wait
    else
        echo "scheduler $scheduler is unsupported" >&2
        exit 1
    fi
}

do_node() {
    if [ "$cpumapper" = "hwloc" ] ; then
        N=$(hwloc-calc --number-of pu machine:0)
        ntasks=$((N / (subdivide_tasks*job_weight)))
        if [ $((N % (subdivide_tasks*job_weight))) != 0 ] ; then
            echo "Warning: number of PUs not a multiple of subdivide_tasks*job_weight.  Scheduling only $ntasks tasks" >&2
        fi
        echo "Starting $ntasks tasks (task loops)"
        children=()
        for i in `seq 0 $((ntasks-1))` ; do
            echo "Starting task $i (task loop)"
            "$me" "${keep_args[@]}" --mode task --cwd "${cwd}" "${script_args[@]}" &
            children=("${children[@]}" $!)
        done
        trap "kill -HUP ${children[*]} ; echo Aborting [$mode]" 2 HUP
        echo "Waiting for tasks to complete"
        wait
    else
        echo "cpumapper $cpumapper is unsupported" >&2
        exit 1
    fi
}

acquire_file() {
    # now comes the hard stuff.
    unset INFILE
    INFILE=`ls -U ${TODO_DIR} | grep '^[0-9]*$' | head -n 100 | sort -R | head -n 1`
    if [ -z "${INFILE}" ] ; then
        echo "${TODO_DIR} is empty"
        exit 0
    fi
    BASEFILE=`basename "${INFILE}"`
    mv "${TODO_DIR}/${BASEFILE}" "${DOING_DIR}/${BASEFILE}"
}

acquire_and_do() {
    while ! acquire_file ; do
        delay=$((RANDOM / 1000))
        if [ ! -z "${INFILE}" ] ; then
            echo "Failed to acquire $INFILE (lost race). sleep $delay" >& 2
        fi
        sleep $delay
    done
    echo "Acquired $BASEFILE"
    echo "Starting $subdivide_tasks subtasks"
    children=()
    for i in `seq 0 $((subdivide_tasks - 1))` ; do
        subtask=$((BASEFILE + i))
        echo "Running:  ${script_args[@]} $subtask"
        "${script_args[@]}" "$subtask" &
        children=("${children[@]}" $!)
        echo "Subtask $subtask is processed by PID $!"
    done
    echo "Waiting for subtasks to complete"
    trap "kill -HUP ${children[*]} ; echo Aborting [$mode] ; sleep 1" 2 HUP
    failed=()
    for i in `seq 0 $((subdivide_tasks - 1))` ; do
        subtask=$((BASEFILE + i))
        child="${children[$i]}"
        wait $child
        rc=$?
        if [ $rc != 0 ] ; then
            echo "Subtask $subtask failed"
            failed=("${failed[@]}" $subtask)
        else
            echo "Subtask $subtask succeeded"
        fi
    done
    if [ "${#failed[@]}" -gt 0 ] ; then
        echo "${#failed[@]} failed subtasks: ${failed[@]}" >&2
        echo "Marking task $BASEFILE as failed"
        mv "${DOING_DIR}/${BASEFILE}" "${FAILED_DIR}/${BASEFILE}"
        exit 1
    fi
    echo "Marking task $BASEFILE as done"
    mv "${DOING_DIR}/${BASEFILE}" "${DONE_DIR}/${BASEFILE}"
    echo "Quiet period -- sleep $afterjob_delay"
    sleep $afterjob_delay
}

do_task() {
    if [ "$repeat" = 0 ] ; then
        while true ; do
            if ! acquire_and_do ; then
                echo "task $BASEFILE failed, aborting" >&2
                break
            fi
        done
    else
        for i in `seq 1 $repeat` ; do
            if ! acquire_and_do ; then
                echo "task $BASEFILE failed, aborting" >&2
                break
            fi
        done
    fi
}

if [ "$mode" = "setup" ] ; then
    do_setup
elif [ "$mode" = "job" ] ; then
    do_job
elif [ "$mode" = "node" ] ; then
    cd "$cwd"
    do_node
elif [ "$mode" = "task" ] ; then
    cd "$cwd"
    do_task
else
    echo "bad mode $mode" >&2
    exit 1
fi
