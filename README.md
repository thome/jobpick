jobpick -- A simple and stupid job picker in shell
==================================================

This program uses a task pool that is created by a user.

The task pool is made of unique task identifiers, originally in the `todo/`
subdirectory.

Task identifiers are expected to be integers, a priori consecutive.

This program is meant to be concurrency-safe: a given task will be
processed only once.

Task lifetime
-------------

Tasks are zero-sized text files, originally in the `todo/` subdirectory.

When a task is being processed, it is moved to the `doing/` subdirectory.

When it completes successfully, it is moved to the `done/` subdirectory.

When it finished with an error, it is moved to the `failed/` subdirectory.

Task identifiers that linger in the `doing/` subdirectory for an absurdly
long time a priori correspond to jobs that were interrupted and did not
have a chance to mark the task as failed.

Modes of operation
------------------

Several modes of operations are known to jobpick:

* task -- this is the smallest level. Executing `jobpick --mode task
  [...]` will pick a task and process it.

* node -- this will fork several *tasks*; see the section "How many tasks
  per node?"

* job -- assuming we're working under a known job scheduler, this will spawn
  *node* runs, one on each node we've been assigned.


How many tasks per node?
------------------------

The basic behaviour is to spawn one task per hyperthreaded core on the
machine. It is subject to change, see the source file and especially the
`do_node` function.

This being said, there is one argument that can be used to control this
behaviour. With `--subdivide-tasks $k`, we handle task `N` in the list
as a cluster of subtasks `N` to `N+k-1`. All subtasks are processed
in parallel. Not that the exit status of task `N` is 0 (success) if and
only if all the subtasks completed successfully. Another argument in the
same spirit is `--job-weight`, which runs one task (or subtask) per group
of some number of hyperthreaded cores.

`--subdivide-tasks` and `--job-weight` provide similar functionality.
The `--job-weight` option is more versatile, in the sense that it lets
the script decide what to do with the task identifier. Yet, provided the
script (of `job-weight` equal to `k`) spawns `k` tasks that each work on
the task identifiers `k*task_id` to `k*task_id+k-1`, we may conceivably
use both to achieve exactly the same thing. In this scenario, the
differences are:

*   With `--subdivide-tasks`, one instance of the script is run for each
    task. If its startup cost is high, this may be inadapted.
*   With `--job-weight`, we rely on the script to properly account for
    the exit status of the work it schedules.

Termination
-----------

Tasks exit successfully when the `todo/` list is empty.

Complete command line arguments
===============================

The general syntax is:

    pick.sh [options...] [script name] [script arguments]

For example

    pick.sh --repeat 2 ~/foo.sh ~/foo-input.txt

* `--job-queue-path`
  Path where the `todo/` `doing/` `done/` and `failed/` subdirectories
  are found.

* `--repeat`
  How many times a task intends to run. Use 0 to run indefinitely.

* `--afterjob-delay`
  By default, a `sleep` is introduced after each task termination, to
  eliminate the risk of uncontrolled spinning. This controls the duration
  of this delay

* `--scheduler`
  Which scheduler is being used. Currently only OAR is supported.

* `--cpumapper`
  Which software back-end is used to count the number of cores and so on.
  Currently we use `hwloc` only.

* `--cwd`
  Path from which the script is to be run. This is also used internally.
  Defaults to `$PWD`.

* `--subdivide-tasks`
  See "How many tasks per node?"

* `--job-weight`
  See "How many tasks per node?"

* `--mode`
  Request running the given mode explicitly. For debugging, `--mode node`
  or `--mode task` can be useful.
